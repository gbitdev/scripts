#!/usr/bin/env bash
git clone --depth=1 https://github.com/DivanteLtd/vue-storefront-api.git
cd vue-storefront-api
chmod -R 777 ./docker/elasticsearch && chown -R 1000:1000 ./docker/elasticsearch/data
docker-compose -f docker-compose.yml -f docker-compose.nodejs.yml up -d && \
    docker-compose -f docker-compose.yml -f docker-compose.nodejs.yml exec app yarn restore && \
    docker-compose -f docker-compose.yml -f docker-compose.nodejs.yml exec app yarn migrate &

cp config/default.json config/local.json 

git clone --depth=1  https://github.com/magento/magento2-sample-data.git var/magento2-sample-data
wait