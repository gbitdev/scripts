#!/usr/bin/env bash
#
mkdir -p $HOME/.byobu
cat > $HOME/.byobu/.tmux.conf << EOL
set -g default-shell /usr/bin/fish
set -g default-command /usr/bin/fish
EOL
