#!/usr/bin/env bash

git clone https://github.com/DivanteLtd/vue-storefront.git && cd vue-storefront 
cp config/default.json config/local.json
docker-compose up -d &

wait
