#!/usr/bin/env bash

apk add byobu fish nano wget curl htop nmap ansible
pip3 install docker docker-compose
curl -sSL http://bit.ly/byobu-fish | sh