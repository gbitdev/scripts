#!/usr/bin/env bash

curl -sSL https://gitlab.com/gbitdev/scripts/raw/master/vue-storefront-api.sh | bash &
curl -sSL https://gitlab.com/gbitdev/scripts/raw/master/vue-storefront.sh | bash &
wait